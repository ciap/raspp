import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _b8d3ca6e = () => interopDefault(import('../pages/boxs.vue' /* webpackChunkName: "pages/boxs" */))
const _3206021d = () => interopDefault(import('../pages/cal.vue' /* webpackChunkName: "pages/cal" */))
const _f21fa52a = () => interopDefault(import('../pages/data.vue' /* webpackChunkName: "pages/data" */))
const _27075ec6 = () => interopDefault(import('../pages/mapa.vue' /* webpackChunkName: "pages/mapa" */))
const _3f3cb761 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/boxs",
    component: _b8d3ca6e,
    name: "boxs"
  }, {
    path: "/cal",
    component: _3206021d,
    name: "cal"
  }, {
    path: "/data",
    component: _f21fa52a,
    name: "data"
  }, {
    path: "/mapa",
    component: _27075ec6,
    name: "mapa"
  }, {
    path: "/",
    component: _3f3cb761,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
